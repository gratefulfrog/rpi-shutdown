#!/usr/bin/python3

""" This implements a propre shutdown for the respberry pi.
Configuration:
* Select the pin you want to use and put its BCN ID number 
  at the definition of offPin (below).

* save this file, 'shutdown.py' to /home/pi/shutdown.py or where you like.

* edit the file  /etc/rc.local and add the following line at the end. 
  NOTE: you need to edit with sudo. Use the right path if you saved
  the file to a different location and don't forget the AMPERSAND '&' at 
  the end of the line!

    sudo python3 /home/pi/shutdown.py &

* that's all there is!
"""

import time
import subprocess
import RPi.GPIO as GPIO

offPin = 4  # use any pin ID you like here! 

def onFall(unusedPin):
    time.sleep(1)
    subprocess.run(['sudo','halt','-p'])

def waitForGnd():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(offPin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
    GPIO.add_event_detect(offPin,GPIO.FALLING,onFall)
    while True:
        time.sleep(1e6)
        
if __name__ == '__main__':
    waitForGnd()
