# rpi shutdown

Code and doc to  create an ON/OFF switch for the raspberry pi 3B+

Once implemented, a momentary Normally Open pushbutton is connected between the offPin and ground.

One pulse of the pushbutton will shutdown the raspberry.

The html file included explains all this.

Instructions are also provided for th implementation of an ON pushbutton as
well as a LED 'running' indicator.